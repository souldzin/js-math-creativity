import React from 'react';
import ReactDOM from 'react-dom';

const Index = () => {
  return <>
    <header>
      <h1>JavaScript Math Creativity!</h1>
    </header>
    <p>Here's the demos</p>
    <ul>
      <li>
        <a href="./demos/towards-us.html">towards-us</a>
      </li>
      <li>
        <a href="./demos/hello-cube.html">hello-cube</a>
      </li>
    </ul>
  </>
};

ReactDOM.render(<Index />, document.getElementById('root'));
