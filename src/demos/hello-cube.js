import {
  BoxGeometry,
  Mesh,
  MeshBasicMaterial
} from 'three';
import start from 'three-boot';

start(({ camera, addObject }) => {
  camera.position.z = 5;

  let count = 0;

  addObject({
    speed: Math.PI / 2,
    $create() {
      const geometry = new BoxGeometry(3, 3, 3);
      const material = new MeshBasicMaterial({ color: 0x22dd99, wireframe: true });
      return new Mesh(geometry, material);
    },
    $onEnterFrame(time) {
      const offset = time * this.speed;

      this.$mesh.rotation.x += offset;
      this.$mesh.rotation.y += offset;
    },
  });
});
