const HtmlWebpackPlugin = require('html-webpack-plugin');

const OUTPUT_DIR = `${__dirname}/public`;

function appConfig(name) {
  return {
    entry: `./src/demos/${name}.js`,
    output: {
      path: `${OUTPUT_DIR}/demos`,
      filename: `${name}.bundle.js`
    },
    plugins: [
      new HtmlWebpackPlugin({
        title: name,
        filename: `${name}.html`,
        template: `./src/demo.html`,
      })
    ]
  };
}

function homeConfig() {
  return {
    entry: './src/index.js',
    output: {
      path: `${OUTPUT_DIR}`,
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
          }
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader'],
        }
      ],
    },
    plugins: [
      new HtmlWebpackPlugin({
        filename: 'index.html',
        template: './src/index.html'
      })
    ]
  }
}

module.exports = [
  homeConfig(),
  appConfig('hello-cube'),
  appConfig('towards-us'),
];
